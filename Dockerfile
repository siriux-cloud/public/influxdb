ARG INFLUXDB_VERSION=2.7.4

# Agregamos Step Cli para iteractuar con Step CA
FROM smallstep/step-cli as step

FROM influxdb:${INFLUXDB_VERSION}

# Instalar step
COPY --from=step /usr/local/bin/step /usr/local/bin/

RUN apt-get update && \
    apt-get install -y wget && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Instalamos gomplate para manipular templates
ARG GOMPLATE_VERSION="3.11.5"
RUN wget https://github.com/hairyhenderson/gomplate/releases/download/v${GOMPLATE_VERSION}/gomplate_linux-amd64 -O /usr/bin/gomplate && \
    chmod +x /usr/bin/gomplate

ARG YQ_VERSION="4.35.2"
RUN wget https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64 -O /usr/bin/yq && \
    chmod +x /usr/bin/yq

# Copiamos archivos del usuasrio root
COPY root/ /
